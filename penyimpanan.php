<?php
//memasukkan file config.php
include('koneksi.php');
?>

<!doctype html>
<html class="no-js" lang="zxx">

<head>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>RS SANTA MARIA PEKANBARU </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="manifest" href="site.webmanifest">
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/lng.png">

        </style>
        <!-- CSS here -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="assets/css/flaticon.css">
        <link rel="stylesheet" href="assets/css/slicknav.css">
        <link rel="stylesheet" href="assets/css/animate.min.css">
        <link rel="stylesheet" href="assets/css/magnific-popup.css">
        <link rel="stylesheet" href="assets/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="assets/css/themify-icons.css">
        <link rel="stylesheet" href="assets/css/slick.css">
        <link rel="stylesheet" href="assets/css/nice-select.css">
        <link rel="stylesheet" href="assets/css/style.css">
    </head>

<body>


    <!-- Preloader Start -->
    <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src="pidum.png" alt="">
                </div>
            </div>
        </div>
    </div>
    <!-- Preloader Start -->

    <header>
        <!-- Header Start -->
        <div class="header-area header-transparrent ">
            <div class="main-header header-sticky">
                <div class="container">
                    <div class="row align-items-center">
                        <!-- Logo -->
                        <div class="col-xl-2 col-lg-2 col-md-2">
                            <div class="logo">
                                <a href="index.php"><img src="pidum.png" alt="image" heigth="200" width="200"></a>
                            </div>
                        </div>
                        <div class="col-xl-8 col-lg-8 col-md-8">
                            <!-- Main-menu -->
                            <div class="main-menu f-right d-none d-lg-block">
                                <nav>
                                    <ul id="navigation">
                                         <li><a href="index.php">HOME    </a></li>
                                         <li><a href="penyimpanan.php"> DAFTAR NAMA TERSANGKA</a></li>
                                          <li><a href="penyimpanan.php">  DAFTAR NAMA-NAMA JAKSA</a></li>
                                    </ul>
                                    </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-md-3">
                            <div class="header-right-btn f-right d-none d-lg-block">
                                <a href="pesan.php" class="btn header-btn">HERE WE GO</a>
                            </div>
                        </div>
                        <!-- Mobile Menu -->
                        <div class="col-12">
                            <div class="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header End -->

    </header>
    <main>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <form action="" method="get">

                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>

                        <img src="logo.png" alt="image" align="right" width="200" height="200">
                        <td colspan="2">
                            <h2 align="center">DAFTAR NAMA JAKSA</h2>
                        </td>
                        <br>
                        <br>

                        <label>Masukkan Kata Kunci :</label>
                        <input type="text" name="cari" />
                        <input type="submit" name="submit" value="Cari" />
                    </form>
                    <hr>

                    <br>
                    <table class="table  table-hover table-sm table-bordered">
                        <thead class="thead-dark">
                            <tr>
                                <th>NO</th>
                                <th>ID Kasus</th>
                                <th>Nama Tersangka</th>
                                <th>Nomor Surat</th>
                                <th>Tanggal Penyidikan</th>
                                <th>File Tersangka</th>
                                <th>AKSI</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php

                            if (isset($_GET['cari'])) {
                                $cari = $_GET['cari'];
                                //query pencarian data
                                $sql = mysqli_query($conn, "SELECT * FROM kasus_pidum WHERE nama like '%" . $cari . "%' OR no like '%" . $cari . "%' ORDER BY id DESC")
                                    or die(mysqli_error($conn));
                                //jika query diatas menghasilkan nilai > 0 maka menjalankan script di bawah if...
                                if (mysqli_num_rows($sql) > 0) {
                                    //membuat variabel $no untuk menyimpan nomor urut
                                    $no = 1;
                                    //melakukan perulangan while dengan dari dari query $sql
                                    while ($data1 = mysqli_fetch_assoc($sql)) {
                                        //menampilkan data perulangan
                                        ?>
                 <tr bgcolor=#DB7093">;
                        <td>$no </td>
                        <td><?=$data1["id_pc"]?></td>
                        <td><?=$data1["nomor_surat"]?></td>
                        <td><?=$data1['tgl_penyidikan']?></td>
                        <td> 
                            <a href="file/<?=$data1["upload_file"]?>">
                            <button type="button"> Dokumen </button>
                            </a></td>
                        <td>
                            <a href="update.php?id=' . $data['id_tamu'] . '" class="badge badge-warning">Edit</a>
                            <a href="delete.php?id=' . $data['id_tamu'] . '" class="badge badge-danger" onclick="return confirm(\'Yakin ingin menghapus data ini?\')">Delete</a>
                        </td>
                    </tr>
                    <?php
                                        $no++;
                                    }
                                    //jika query menghasilkan nilai 0
                                } else {
                                    echo '
                <tr>
                    <td colspan="6">Tidak ada data.</td>
                </tr>
                ';
                                }
                            } else {
                                //pagination
                                $halaman = 5;    //jumlah data setiap halaman
                                $page = isset($_GET["halaman"]) ? (int) $_GET["halaman"] : 1;    //membuat url halaman. default 1
                                $mulai = ($page > 1) ? ($page * $halaman) - $halaman : 0;        //perhitungan halaman. default 0
                                //menghuting jumlah data
                                $result = mysqli_query($conn, "SELECT * FROM kasus_pidum");
                                $total = mysqli_num_rows($result);
                                $total_page = ceil($total / $halaman);
                                $sql = mysqli_query($conn, "SELECT * FROM kasus_pidum ORDER BY id_pc ASC LIMIT $mulai, $halaman") or die(mysqli_error($conn));
                                $no = $mulai + 1;

                                //$sql = mysqli_query($conn, "SELECT * FROM tamu ORDER BY id DESC") or die(mysqli_error($koneksi));
                                //jika query diatas menghasilkan nilai > 0 maka menjalankan script di bawah if...
                                if (mysqli_num_rows($sql) > 0) {
                                    //membuat variabel $no untuk menyimpan nomor urut
                                    $no = 1;
                                    //melakukan perulangan while dengan dari dari query $sql
                                    while ($data = mysqli_fetch_assoc($sql)) {
                                        //menampilkan data perulangan
                                        echo '
                    <tr bgcolor=#DB7093">
                        <td>' . $no . '</td>
                        <td>' . $data['id_pc'] . '</td>
                        <td>' . $data['nama'] . '</td>
                        <td>' . $data['nomor_surat'] . '</td>
                        <td>' . $data['tgl_penyidikan'] . '</td>
                        <td>
                            <a href="file/$data["upload_file"]">
                            <button type="button"> Dokumen </button>
                            </a></td>
                        <td>
                            <a href="update.php?id=' . $data['id_pc'] . '" class="badge badge-warning">Edit</a>
                            <a href="delete.php?id=' . $data['id_pc'] . '" class="badge badge-danger" onclick="return confirm(\'Yakin ingin menghapus data ini?\')">Delete</a>
                        </td>
                    </tr>
                    ';
                                        $no++;
                                    }
                                    //jika query menghasilkan nilai 0
                                } else {
                                    echo '
                <tr>
                    <td colspan="6">Tidak ada data.</td>
                </tr>
                ';
                                }
                            }


                            ?>

                        <tbody>

                    </table>

                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item"><a class="page-link" href="penyimpanan.php?page=" .($page-1).>Previous</a></li>
                            <?php
                            for ($i = 1; $i <= $no; $i++) {
                            ?>
                                <li class="page-item"><a class="page-link" href="?halaman=<?php echo $i; ?>"><?php echo $i; ?></a></li>

                                <a href="</a>
           
          <?php } ?>
              <li class=" page-item"><a class="page-link" href="penyimpanan.php?page=" .($page+1).>Next</a></li>

                        </ul>
                    </nav>

                </div>

                <br>
                <br>
                <br>
                <br>

                <br>
                <br>
                <br>
                <br>
                <marquee direction="right" scrollamount="2" align="center" behavior="alternate">
                    <h2 align="center">SATYA ADHI WICAKSANA</h2>
                </marquee>



                <!-- footer-bottom aera -->
                <div class="footer-bottom-area footer-bg">
                    <div class="container">
                        <div class="footer-border">
                            <div class="row d-flex align-items-center">
                                <div class="col-xl-12 ">
                                    <div class="footer-copy-right text-center">
                                        <p>
                                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                            Copyright &copy;<script>
                                                document.write(new Date().getFullYear());
                                            </script> Universitas Islam Riau | <i class="ti-heart" aria-hidden="true"></i> by <a href="#" target="_blank">Silvia Miftaviana</a>
                                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer End-->

            </footer>

            <!-- JS here -->

            <!-- All JS Custom Plugins Link Here here -->
            <script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
            <!-- Jquery, Popper, Bootstrap -->
            <script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
            <script src="./assets/js/popper.min.js"></script>
            <script src="./assets/js/bootstrap.min.js"></script>
            <!-- Jquery Mobile Menu -->
            <script src="./assets/js/jquery.slicknav.min.js"></script>

            <!-- Jquery Slick , Owl-Carousel Plugins -->
            <script src="./assets/js/owl.carousel.min.js"></script>
            <script src="./assets/js/slick.min.js"></script>
            <!-- Date Picker -->
            <script src="./assets/js/gijgo.min.js"></script>
            <!-- One Page, Animated-HeadLin -->
            <script src="./assets/js/wow.min.js"></script>
            <script src="./assets/js/animated.headline.js"></script>
            <script src="./assets/js/jquery.magnific-popup.js"></script>

            <!-- Scrollup, nice-select, sticky -->
            <script src="./assets/js/jquery.scrollUp.min.js"></script>
            <script src="./assets/js/jquery.nice-select.min.js"></script>
            <script src="./assets/js/jquery.sticky.js"></script>

            <!-- contact js -->
            <script src="./assets/js/contact.js"></script>
            <script src="./assets/js/jquery.form.js"></script>
            <script src="./assets/js/jquery.validate.min.js"></script>
            <script src="./assets/js/mail-script.js"></script>
            <script src="./assets/js/jquery.ajaxchimp.min.js"></script>

            <!-- Jquery Plugins, main Jquery -->
            <script src="./assets/js/plugins.js"></script>
            <script src="./assets/js/main.js"></script>

</body>

</html>