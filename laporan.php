<!doctype html>
<html class="no-js" lang="zxx">

<head>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>SILVIA MIFTAVIANA </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="manifest" href="site.webmanifest">
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">


        <style type="text/css">
            #main {
                background-image: url("parallax_12.jpg");
            }
        </style>

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="assets/css/flaticon.css">
        <link rel="stylesheet" href="assets/css/slicknav.css">
        <link rel="stylesheet" href="assets/css/animate.min.css">
        <link rel="stylesheet" href="assets/css/magnific-popup.css">
        <link rel="stylesheet" href="assets/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="assets/css/themify-icons.css">
        <link rel="stylesheet" href="assets/css/slick.css">
        <link rel="stylesheet" href="assets/css/nice-select.css">
        <link rel="stylesheet" href="assets/css/style.css">
    </head>

<body>
    <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src="logo.png" alt="">
                </div>
            </div>
        </div>
    </div>

    <header>
        <div class="header-area header-transparrent ">
            <div class="main-header header-sticky">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-xl-2 col-lg-2 col-md-1">
                            <div class="logo">
                                <a href="index.php"><img src="pidum.png" alt="image" heigth="100" width="300"></a>
                            </div>
                        </div>
                        <div class="col-xl-8 col-lg-8 col-md-8">
                            <div class="main-menu f-right d-none d-lg-block">
                                <nav>
                                    <ul id="navigation">
                                        <li><a href="index.php">HOME </a></li>
                                        <li><a href="penyimpanan.php"> DAFTAR NAMA TERSANGKA</a></li>
                                        <li><a href="penyimpanan.php"> DAFTAR NAMA-NAMA JAKSA</a></li>

                                        </li>

                                    </ul>
                                    </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-md-3">
                            <div class="header-right-btn f-right d-none d-lg-block">
                                <a href="logout.php" class="btn header-btn">Logout</a>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <main>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <form action="file.php" method="POST">

                        <body background-color="grey">
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <img src="logo.png" alt="image" align="right" width="500" height="500">
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <table>
                                <td colspan="2">
                                    <h2 align="center">LAPORAN KASUS PIDUM</h2>
                                </td>

                                <br>
                                <div class="form-group row">
                                    <tr>
                                        <td>
                                            <label for="nama" class="col-sm-4 col-form-label">ID tersangka</label>
                                        </td>
                                        <td>
                                            <input type="number" id="id_tsk" name="id_tsk" placeholder="input ID" style="width: 200px;" required>
                                        </td>
                                    </tr>
                                </div>
                                <tr>
                                    <div class="form-group row">
                                        <td>
                                            <label for="nama" class="col-sm-4 col-form-label">Nama Tersangka</label>
                                        </td>
                                        <div class="col-sm-4">
                                            <td>
                                                <input type="text" id="nama_tsk" name="nama_tsk" placeholder="input nama" style="width: 200px;" required>
                                            </td>
                                        </div>
                                    </div>
                                </tr>
                                <tr>
                                    <div class="form-group row">
                                        <td><label for="inputTTL" class="col-sm-4 col-form-label">Tanggal Lahir</label></td>
                                        <div class="col-sm-4">
                                            <td><input type="date" class="form-control" id="tgl_lahir" name="tgl_lahir" placeholder="Masukkan tanggal Lahir"></td>
                                        </div>
                                    </div>
                                </tr>
                                <tr>
                                    <div class="form-group row">
                                        <td><label for="kewarganegaraan" class="col-sm-4 col-form-label">Kewarganegaraan</label></td>
                                        <td>

                                            <select name="kewarganegaraan" id="">
                                                <option value="indonesia">Indonesia</option>
                                                <option value="singapura">WN Asing</option>
                                            </select>

                                        </td>
                                    </div>
                                </tr>
                                <tr>
                                    <div class="form-group row">
                                        <td>
                                            <label for="alamat_tinggal" class="col-sm-4 col-form-label">Alamat Tinggal</label>
                                        </td>
                                        <td>

                                            <textarea name="alamat_tinggal" placeholder="cantumkan alamat tinggal tersangka"></textarea>

                                        </td>
                                    </div>
                                </tr>

                                <tr>
                                    <div class="form-group row">
                                        <td>
                                            <label for="agama" class="col-sm-4 col-form-label">Agama</label>
                                        </td>
                                        <div class="col-sm-4">
                                            <td>
                                                <select name="kewarganegaraan" id="" style="width: 200px;">
                                                    <option value="islam">Islam</option>
                                                    <option value="kristen">Kristen</option>
                                                    <option value="hindu">Hindu</option>
                                                    <option value="budha">Budha</option>
                                                </select>
                                            </td>
                                        </div>
                                    </div>
                                </tr>
                                <tr>
                                    <div class="form-group row">
                                        <td>
                                            <label for="pekerjaan" class="col-sm-4 col-form-label">Pekerjaan</label>
                                        </td>
                                        <div class="col-sm-4">
                                            <td>
                                                <select name="pekerjaan" id="" style="width: 200px;">
                                                    <option value="pns">PNS</option>
                                                    <option value="honorer">Honorer</option>
                                                    <option value="wiraswasta">Wiraswasta</option>
                                                    <option value="tidakbekerja">Tidak bekerja</option>
                                                </select>
                                            </td>
                                        </div>
                                    </div>
                                </tr>
                                <tr>
                                    <div class="form-group row">
                                        <td>
                                            <label for="nik" class="col-sm-4 col-form-label">NIK</label>
                                        </td>
                                        <div class="col-sm-4">
                                            <td>
                                                <input type="number" id="nik" name="nik" placeholder="input NIK" style="width: 200px;" required>
                                            </td>
                                        </div>
                                    </div>
                                </tr>
                                <tr>
                                    <div class="form-group row">
                                        <td>
                                            <label for="inputTTL" class="col-sm-4 col-form-label">Tanggal penyidikan</label>
                                        </td>
                                        <div class="col-sm-4">
                                            <td>
                                                <input type="date" class="form-control" id="tgl_penyidikan" name="tgl_penyidikan" placeholder="Masukkan tanggal penyidikan">
                                            </td>
                                        </div>
                                    </div>
                                </tr>
                                <tr>
                                    <div class="form-group row">
                                        <td>
                                            <label for="jenis_pidana" class="col-sm-4 col-form-label">Jenis Pidana</label>
                                        </td>
                                        <div class="col-sm-4">
                                            <td>
                                                <select name="pekerjaan" id="" style="width: 200px;">
                                                    <option value="napza">Kasi NAPZA</option>
                                                    <option value="tpul">Kasi KAMNEG/TPUL</option>
                                                    <option value="oharda">Kasi OHARGA</option>
                                                </select>
                                            </td>
                                        </div>
                                    </div>
                                </tr>
                                <tr>
                                    <div class="form-group row">
                                        <td>
                                            <label for="nama" class="col-sm-4 col-form-label">Upload File</label>
                                        </td>
                                        <div class="col-sm-4">
                                            <td>
                                                <input type="file" id="upload_file" name="upload_file" style="width: 200px;" required>
                                            </td>
                                        </div>
                                    </div>
                                </tr>
                                <tr>
                                    <td>
                                        <button type="reset">
                                            <font color="blue"><b>RESET</b></font>
                                        </button>
                                    </td>
                                    <td>
                                        <button type="submit" name="pesan">
                                            <font color="blue"><b>PROSES</b></font>
                                        </button>
                                    </td>
                                </tr>
                            </table>

                </div>
            </div>
        </div>
        <br> <br>
        <hr style="border: 3px solid black;width: 100%;">



        <div class="footer-bottom-area footer-bg">
            <div class="container">
                <div class="footer-border">
                    <div class="row d-flex align-items-center">
                        <div class="col-xl-12 ">
                            <div class="footer-copy-right text-center">
                                <p>
                                    Copyright &copy;<script>
                                        document.write(new Date().getFullYear());
                                    </script> by <i class="ti-heart" aria-hidden="true"></i> Silvia Miftaviana <a href="#" target="_blank">Universitas Islam Riau</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>


        </footer>



        <script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
        <script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
        <script src="./assets/js/popper.min.js"></script>
        <script src="./assets/js/bootstrap.min.js"></script>
        <script src="./assets/js/jquery.slicknav.min.js"></script>
        <script src="./assets/js/owl.carousel.min.js"></script>
        <script src="./assets/js/slick.min.js"></script>
        <script src="./assets/js/gijgo.min.js"></script>
        <script src="./assets/js/wow.min.js"></script>
        <script src="./assets/js/animated.headline.js"></script>
        <script src="./assets/js/jquery.magnific-popup.js"></script>

        <script src="./assets/js/jquery.scrollUp.min.js"></script>
        <script src="./assets/js/jquery.nice-select.min.js"></script>
        <script src="./assets/js/jquery.sticky.js"></script>

        <script src="./assets/js/contact.js"></script>
        <script src="./assets/js/jquery.form.js"></script>
        <script src="./assets/js/jquery.validate.min.js"></script>
        <script src="./assets/js/mail-script.js"></script>
        <script src="./assets/js/jquery.ajaxchimp.min.js"></script>

        <script src="./assets/js/plugins.js"></script>
        <script src="./assets/js/main.js"></script>

</body>

</html>